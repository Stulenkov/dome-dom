import ScrollBooster from 'scrollbooster';

// Переменная, регулирующая кликабельность ссылок
let PointerDownBoolean;
if(window.innerWidth > 768){
  PointerDownBoolean = true;
}
else{
  PointerDownBoolean = false;
}
//

$.each('.js-booster-x', el => {
  new ScrollBooster({
    viewport: el,
    content: el.querySelector('.js-booster__inner'),
    scrollMode: 'transform',
    direction: 'horizontal',
    emulateScroll: false,
    pointerDownPreventDefault: PointerDownBoolean,
  });
})