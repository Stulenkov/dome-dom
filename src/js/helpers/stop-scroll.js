const body = document.querySelector('body');
let top = 0;
let lock = false;

const disable = () => {
  if (lock) return false;
  lock = true;

  if (
    window.loco &&
    !window.loco.isMobile &&
    typeof window.scroll === 'object'
  ) {
    window.scroll.stop();
  } else {
    top = window.pageYOffset;
    body.style.top = `-${top}px`;
    body.classList.add('no-scroll');
  }
};

const enable = () => {
  if (!lock) return false;
  lock = false;

  if (
    window.loco &&
    !window.loco.isMobile &&
    typeof window.scroll === 'object'
  ) {
    window.scroll.start();
  } else {
    body.style.top = '';
    body.classList.remove('no-scroll');

    setTimeout(() => {
      window.scrollTo(0, top);
    }, 0);
  }
};

export default { disable, enable };
