$.qs('body').style.visibility = 'visible';

//Init
import '~/js/init/scrollbooster';
import '~/js/init/modal';

// Blocks
import '~/blocks/modal/modal';
import '~/blocks/scroll/scroll';
import '~/blocks/mobile-menu/mobile-menu';