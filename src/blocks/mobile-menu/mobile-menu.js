import { close } from '~/blocks/modal/modal';

$.delegate(`.js-menu-btn`, (e, el) => {
    const modal = $.qs('[data-modal="mobile-menu"]');
    close(modal)
})