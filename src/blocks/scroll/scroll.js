import LocomotiveScroll from 'locomotive-scroll';

let loco;
window.addEventListener('DOMContentLoaded', () => {
  const scrollContainer = $.qs('#js-scroll');
  if (!scrollContainer) return false;
  // Scroll instance
  loco = new LocomotiveScroll({
    el: scrollContainer,
    smooth: true,
    getDirection: true
  });

  window.loco = loco;

  // Save scroll data
  loco.on('scroll', e => {
    window.scroll.data = e;
    document.dispatchEvent(new CustomEvent("customScroll", {
      detail: {
        offsetY: e.scroll.y,
        direction: e.direction
      }
    }));
  });

  // ScrollTo anchors
  $.delegate(`[data-anchor]`, (e, el) => {
    $.each(`[data-target]`, section => {
      if(el.dataset.anchor == section.dataset.target){
        loco.scroll.scrollTo(section);
      }
    })
  })
});

// Update scroll on full page load
window.addEventListener('load', () => {
  if (loco) loco.update();
});

document.addEventListener('scrollUpdate', () => {
  if (loco) loco.update();
});